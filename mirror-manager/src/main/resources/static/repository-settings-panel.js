define('plugin/mirror-ops/repository-settings', [
                                              'jquery',
                                              'aui',
                                              'util/ajax',
                                              'util/navbuilder',
                                              'model/page-state',
                                              'util/error',
                                              'exports'
                                              ], function (
                                            		  $,
                                            		  AJS,
                                            		  ajax,
                                            		  navBuilder,
                                            		  pageState,
                                            		  errorUtil,
                                            		  exports
                                              ) {

	function getProjectKey() {
		return pageState.getProject().getKey();
	}

	function getRepoSlug() {
		return pageState.getRepository().getSlug();
	}

	function fetchMirrorList(mirrors_box) {

		$.getJSON( getUrlBase() + '/api',
				{
			project_key: getProjectKey(),
			repo_slug: getRepoSlug()
				},
				function(data) {

					$.each(data.mirrors, function(mirror_name, mirror_settings) {

						var mirror_url = mirror_settings.url;
						var mirror_enabled = mirror_settings.enabled;

						var single_mirror_div = $("<div>", {"class": "field-group", "style": "margin-top: 1.5em"});

						var checkbox_id = "id-mirror-" + mirror_name;
						var checkbox_name = "name-mirror-" + mirror_name;

						var single_mirror_description = $("<h3>", {"class": "description"});
						single_mirror_description.text(mirror_name);
						single_mirror_div.append(single_mirror_description);

						var single_mirror_description2 = $("<div>", {"class": "description", "style": "font-weight: bold"});
						single_mirror_description2.text(mirror_url);
						single_mirror_div.append(single_mirror_description2);


						var single_mirror_checkbox_div = $("<div>", {"class": "checkbox"});
						single_mirror_div.append(single_mirror_checkbox_div);


						var single_mirror_checkbox = $("<input>", {"id": checkbox_id, "class": "checkbox", "type": "checkbox", "name": checkbox_name});
						if (mirror_enabled)
							single_mirror_checkbox.attr("checked", "checked");

						single_mirror_checkbox.change(function () {
							
							var checkbox_is_checked = Boolean($(this).attr("checked"));
							$.post( getUrlBase() + '/api/toggle',
									{
								project_key: getProjectKey(),
								repo_slug: getRepoSlug(),
								mirrorName: mirror_name,
								enabledState: checkbox_is_checked
									},
									function(data) {
										renderMirrorList();
									});
						});

						single_mirror_checkbox_div.append(single_mirror_checkbox);

						var single_mirror_label = $("<label>", {"for": checkbox_id});
						single_mirror_label.text("Enable automatic post-receive pushing");
						single_mirror_checkbox_div.append(single_mirror_label);





						var deletion_button = $("<button>", {"class": "aui-button"});
						deletion_button.text("Remove");
						deletion_button.click(function() {
							if (confirm("Are you sure you want to remove this mirror?")) {
								$.ajax(
										getUrlBase() + '/api',
										{
											type: "DELETE",
											data: {
												project_key: getProjectKey(),
												repo_slug: getRepoSlug(),
												mirrorName: mirror_name
											},
											success: function(data) {
												renderMirrorList();
											}
										}
								);
							}
						});
						single_mirror_div.append(deletion_button);


						/*
						// NOT IMPLEMENTED YET
						var connectivity_test_button = $("<button>", {"class": "aui-button"});
						connectivity_test_button.text("Test connectivity");
						connectivity_test_button.click(function() {

							$.getJSON( getUrlBase() + '/api/test',
									{
								project_key: getProjectKey(),
								repo_slug: getRepoSlug(),
								mirror_name: mirror_name
									},
									function(data) {
										alert("[Not implemented] succeeded in testing: " + data);
									}
							);
						});
						single_mirror_div.append(connectivity_test_button);
						*/

						var manual_push_button = $("<button>", {"class": "aui-button"});
						manual_push_button.text("Manual push");
						manual_push_button.click(function() {
							$.ajax(
									getUrlBase() + '/api/test',
									{
										type: "PUT",
										data: {
											project_key: getProjectKey(),
											repo_slug: getRepoSlug(),
											mirrorName: mirror_name
										},
										success: function(data) {
											if (data.success) {
												alert("Succeeded in pushing to mirror: " + data.name);
											} else {
												alert("Failed in pushing to mirror: " + data.error_message);
											}
										}
									}
							);
						});
						single_mirror_div.append(manual_push_button);


						mirrors_box.append(single_mirror_div);
					});
				}
		);
	}


	function renderMirrorList() {

		var mirrors_box = $("#mirror-selection");
		mirrors_box.empty();
		fetchMirrorList(mirrors_box);
	}



	function initAdderButtonBehavior() {
		var adder_button = $("#trigger-gc-button");
		adder_button.click(function() {

			var mirror_name = prompt("Choose an alias for this remote mirror:", "my_mirror")
			var mirror_url = prompt("Specify URL remote mirror: \"" + mirror_name + "\"", "file:///tmp/stash/mirror-test.git")	
			$.post( getUrlBase() + '/api',
					{
				project_key: getProjectKey(),
				repo_slug: getRepoSlug(),
				mirrorName: mirror_name,
				mirrorUrl: mirror_url
					},
					function(data) {
						renderMirrorList();
					});
		});
	}


	function getUrlBase() {
		return AJS.contextPath() + '/rest/stash-mirror/latest';
	}


	exports.onReady = function () {

		initAdderButtonBehavior();

		// FIXME Why is this required?
		setTimeout(function() {
			renderMirrorList();
		}, 0);

	}
});
