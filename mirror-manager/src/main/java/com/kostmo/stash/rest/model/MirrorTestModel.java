package com.kostmo.stash.rest.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class MirrorTestModel {

    @JsonProperty
    private String name;

    @JsonProperty
    private String url;

    @JsonProperty
    private boolean success;
    
    @JsonProperty
    private String error_message;
    
    public MirrorTestModel(String name, String url, boolean success, String error_message) {
        this.name = name;
        this.url = url;
        this.success = success;
        this.error_message = error_message;
    }
}