package com.kostmo.stash.rest.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class MirrorEnabledModel {

    @JsonProperty
    private boolean enabled;
    
    @JsonProperty
    private String url;
    
    public MirrorEnabledModel(String url, boolean enabled) {
        this.url = url;
        this.enabled = enabled;
    }
}