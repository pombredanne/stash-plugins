package com.kostmo.stash.rest;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.stash.hook.repository.RepositoryHookService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.kostmo.stash.rest.model.MirrorModel;

@Path("/api/toggle")
@Produces({MediaType.APPLICATION_JSON})
public class MirrorToggleResource {

	private final ApplicationPropertiesService application_properties_service;
	private final RepositoryService repo_service;
	private final RepositoryHookService hook_service;
	private final PermissionValidationService permissionValidationService;
	MirrorToggleResource(ApplicationPropertiesService applicationPropertiesService, RepositoryService repo_service, RepositoryHookService hook_service, PermissionValidationService permissionValidationService) {
		this.application_properties_service = applicationPropertiesService;
		this.repo_service = repo_service;
		this.hook_service = hook_service;
		this.permissionValidationService = permissionValidationService;
	}

	@POST
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response setSettings(
			@FormParam("project_key") final String project_key,
			@FormParam("repo_slug") final String repo_slug,
			@FormParam("mirrorName") final String mirror_name,
			@FormParam("enabledState") final boolean enabled_state) throws IOException {
		
		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);
		permissionValidationService.validateForRepository(stash_repository, Permission.REPO_ADMIN);
		
		ApiConfigurationResource.updateMirrorEnabledSetting(stash_repository, hook_service, mirror_name, enabled_state);
		
		return Response.ok(new MirrorModel(mirror_name, "blah")).build();
	}
}
