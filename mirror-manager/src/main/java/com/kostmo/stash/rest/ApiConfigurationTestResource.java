package com.kostmo.stash.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.exception.ServerException;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.kostmo.stash.AsyncPostReceivePusher;
import com.kostmo.stash.rest.model.MirrorTestModel;

/**
 * To query this REST API, use this curl command:
 * curl http://ubuntu:7990/stash/rest/stash-mirror/latest/api --user admin:admin
 * 
 * @author kostmo
 *
 */
@Path("/api/test")
@Produces({MediaType.APPLICATION_JSON})
public class ApiConfigurationTestResource {


    private static final Logger log = LoggerFactory.getLogger(ApiConfigurationTestResource.class);

	
	private final ApplicationPropertiesService application_properties_service;
	private final RepositoryService repo_service;
	private final GitCommandBuilderFactory gitCommandBuilderFactory;
    private final PermissionValidationService permissionValidationService;

	ApiConfigurationTestResource(ApplicationPropertiesService applicationPropertiesService, RepositoryService repo_service, GitCommandBuilderFactory gitCommandBuilderFactory, PermissionValidationService permissionValidationService) {
		this.application_properties_service = applicationPropertiesService;
		this.repo_service = repo_service;
		this.gitCommandBuilderFactory = gitCommandBuilderFactory;
		this.permissionValidationService = permissionValidationService;
	}

	@GET
	public Response getSettings(@QueryParam("project_key") final String project_key, @QueryParam("repo_slug") final String repo_slug, @QueryParam("mirror_name") final String mirror_name) throws IOException {

		System.out.println("##### project_key query parameter: " + project_key);
		System.out.println("##### repo_slug query parameter: " + repo_slug);
		System.out.println("##### mirror_name query parameter: " + mirror_name);

		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);

		
		
		// FIXME USE THIS
		Map<String, String> results = AsyncPostReceivePusher.testRepoRemote(application_properties_service, stash_repository);

		final MirrorTestModel apiModel = new MirrorTestModel(mirror_name, "blah", true, null);
		return Response.ok(apiModel).build();
	}

	@PUT
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response setSettings(
			@FormParam("project_key") final String project_key,
			@FormParam("repo_slug") final String repo_slug,
			@FormParam("mirrorName") final String mirror_name) throws IOException {

		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);		
		permissionValidationService.validateForRepository(stash_repository, Permission.REPO_ADMIN);
		
		Collection<String> remotes = new ArrayList<String>();
		remotes.add(mirror_name);
		
		try {
			AsyncPostReceivePusher.pushToRemotes(gitCommandBuilderFactory, stash_repository, remotes);
		} catch (ServerException e) {
			return Response.ok(new MirrorTestModel(mirror_name, null, false, e.getMessage())).build();
		}

		return Response.ok(new MirrorTestModel(mirror_name, null, true, null)).build();
	}
}
