package com.kostmo.stash.rest.model;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class MirrorModel {

    @JsonProperty
    private String name;

    @JsonProperty
    private String url;

    @JsonProperty
    private Map<String, String> mirrors;
    
    public MirrorModel(String name, String url) {
        this.name = name;
        this.url = url;
    }
}