package com.kostmo.stash.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.hook.repository.RepositoryHookService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsBuilder;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.kostmo.stash.AsyncPostReceivePusher;
import com.kostmo.stash.plugin.mirror.MirrorServlet;
import com.kostmo.stash.rest.model.ApiConfigurationModel;
import com.kostmo.stash.rest.model.MirrorModel;

/**
 * To query this REST API, use this curl command:
 * curl http://ubuntu:7990/stash/rest/stash-mirror/latest/api --user admin:admin
 * 
 * @author kostmo
 *
 */
@Path("/api")
@Produces({MediaType.APPLICATION_JSON})
public class ApiConfigurationResource {


    private static final Logger log = LoggerFactory.getLogger(ApiConfigurationResource.class);

	
	public final static String HOOK_KEY = "repo-mirror-pusher";
	
	private final ApplicationPropertiesService application_properties_service;
	private final RepositoryService repo_service;
	private final RepositoryHookService hook_service;
    private final PermissionValidationService permissionValidationService;

	ApiConfigurationResource(ApplicationPropertiesService applicationPropertiesService, RepositoryService repo_service, RepositoryHookService hook_service, PermissionValidationService permissionValidationService) {
		this.application_properties_service = applicationPropertiesService;
		this.repo_service = repo_service;
		this.hook_service = hook_service;
		this.permissionValidationService = permissionValidationService;
	}

	public final static String getModuleKey() {
		return MirrorServlet.PLUGIN_KEY + ":" + HOOK_KEY;
	}

	
	@GET
	public Response getSettings(@QueryParam("project_key") final String project_key, @QueryParam("repo_slug") final String repo_slug) throws IOException {

		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);
		Map<String, String> mirrors = AsyncPostReceivePusher.getRepoRemotes(application_properties_service, stash_repository);
		Settings settings = this.hook_service.getSettings(stash_repository, getModuleKey());
		Map<String, Boolean> enabled_mirrors = getMirrorsEnabled(settings, mirrors);
		
		final ApiConfigurationModel apiModel = new ApiConfigurationModel(mirrors, enabled_mirrors);
		return Response.ok(apiModel).build();
	}

	public static Map<String, Boolean> getMirrorsEnabled(Settings settings, Map<String, String> mirrors) {
		Map<String, Boolean> enabled = new HashMap<String, Boolean>();
		for (Entry<String, String> entry : mirrors.entrySet())
			enabled.put(entry.getKey(), settings != null && settings.getBoolean(entry.getKey(), false));
		return enabled;
	}

	
	public static void updateMirrorEnabledSetting(Repository stash_repository, RepositoryHookService hook_service, String mirror_name, boolean enabled_state) {
		Settings settings = hook_service.getSettings(stash_repository, getModuleKey());
		SettingsBuilder new_settings_builder = hook_service.createSettingsBuilder();
		
		// Restore all other settings entries, skipping over the one to modify
		if (settings != null)
			for (Entry<String, Object> entry : settings.asMap().entrySet())
				if (!entry.getKey().equals(mirror_name))
					new_settings_builder.add(entry.getKey(), (Boolean) entry.getValue());
		
		new_settings_builder.add(mirror_name, enabled_state);
		hook_service.setSettings(stash_repository, getModuleKey(), new_settings_builder.build());
	}

	@POST
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response setSettings(
			@FormParam("project_key") final String project_key,
			@FormParam("repo_slug") final String repo_slug,
			@FormParam("mirrorName") final String mirror_name,
			@FormParam("mirrorUrl") final String mirror_url) throws IOException {
				
		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);
        permissionValidationService.validateForRepository(stash_repository, Permission.REPO_ADMIN);

		AsyncPostReceivePusher.addRepoRemote(application_properties_service, stash_repository, mirror_name, mirror_url);
		updateMirrorEnabledSetting(stash_repository, hook_service, mirror_name, true);
		return Response.ok(new MirrorModel(mirror_name, mirror_url)).build();
	}

	@DELETE
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	public Response setSettings(
			@FormParam("project_key") final String project_key,
			@FormParam("repo_slug") final String repo_slug,
			@FormParam("mirrorName") final String mirror_name) throws IOException {
		
		Repository stash_repository = this.repo_service.getBySlug(project_key, repo_slug);
        permissionValidationService.validateForRepository(stash_repository, Permission.REPO_ADMIN);

		AsyncPostReceivePusher.removeRepoRemote(application_properties_service, stash_repository, mirror_name);
		return Response.ok(new MirrorModel(mirror_name, null)).build();
	}
}
