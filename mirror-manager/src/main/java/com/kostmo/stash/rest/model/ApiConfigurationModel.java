package com.kostmo.stash.rest.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class ApiConfigurationModel {

    @JsonProperty
    private Map<String, MirrorEnabledModel> mirrors;
    
    public ApiConfigurationModel(Map<String, String> mirrors, Map<String, Boolean> enabled_mirrors) {
    	
    	Map<String, MirrorEnabledModel> mirror_list = new HashMap<String, MirrorEnabledModel>();
		for (Entry<String, String> entry : mirrors.entrySet())
			mirror_list.put(entry.getKey(), new MirrorEnabledModel(entry.getValue(), enabled_mirrors.get(entry.getKey())));

        this.mirrors = mirror_list;
    }
}