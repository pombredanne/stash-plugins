package com.kostmo.stash.plugin.mirror;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@SuppressWarnings("serial")
public class MirrorServlet extends HttpServlet {

    public static final String PLUGIN_KEY = "com.kostmo.stash.mirror-manager";
    
    private final PermissionValidationService permissionValidationService;
    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WebResourceManager webResourceManager;

    public MirrorServlet(PermissionValidationService permissionValidationService,
                                          RepositoryService repositoryService, SoyTemplateRenderer soyTemplateRenderer,
                                          WebResourceManager webResourceManager) {
        this.permissionValidationService = permissionValidationService;
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String[] pathParts = pathInfo.substring(1).split("/");
        if (pathParts.length != 2) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String projectKey = pathParts[0];
        String repoSlug = pathParts[1];
        Repository repository = repositoryService.getBySlug(projectKey, repoSlug);
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        doView(repository, req, resp);
    }

    private void doView(Repository repository, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        render(resp,
                "stash.page.gitOperations.repoSettingsPanel",
                ImmutableMap.<String, Object>builder()
                        .put("repository", repository)
                        .build()
        );
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        webResourceManager.requireResourcesForContext("stash.page.repository.settings.git-ops");
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), PLUGIN_KEY + ":mirror-ops-serverside-resources", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }
}
