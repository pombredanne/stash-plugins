package com.kostmo.stash;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.CommandOutputHandler;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import com.kostmo.stash.rest.ApiConfigurationResource;

public class AsyncPostReceivePusher implements AsyncPostReceiveRepositoryHook {

	private static final String REMOTES_CONFIG_SECTION_NAME = "remote";
	private final static String KEY_REMOTES_URL = "url";
	private final static String KEY_REMOTES_MIRROR = "mirror";

	private final GitCommandBuilderFactory gitCommandBuilderFactory;
	private final ApplicationPropertiesService applicationPropertiesService;
	AsyncPostReceivePusher(GitCommandBuilderFactory gitCommandBuilderFactory, ApplicationPropertiesService applicationPropertiesService) {
		this.gitCommandBuilderFactory = gitCommandBuilderFactory;
		this.applicationPropertiesService = applicationPropertiesService;
	}

	public static void pushToRemotes(GitCommandBuilderFactory git_cmd_factory, Repository repository, Collection<String> remotes) {

		CommandOutputHandler<Void> my_command_output_handler = new CommandOutputHandler<Void>() {

			@Override
			public void complete() throws ProcessException {
//				System.out.println("## PUSHER ## COMPLETE");
			}

			@Override
			public void process(InputStream in) throws ProcessException {

				// XXX There doesn't seem to be any output here...
				BufferedReader br = new BufferedReader(new InputStreamReader(in));

				//Read File Line By Line
				String line = null;
				try {
					while ((line = br.readLine()) != null)
						System.out.println("## PUSHER ## LINE: " + line);

					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void setWatchdog(Watchdog arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			@Nullable
			public Void getOutput() {
				// TODO Auto-generated method stub
				return null;
			}
		};	

		// Push to all remote repos
		for (String repo_name : remotes) {
			System.out.println( String.format("Attempting to push to repo \"%s\"...", repo_name) );
			git_cmd_factory.builder(repository).push().repository(repo_name).build(my_command_output_handler).call();
		}
	}
	
	/**
	 * Cross-references the names of remotes that are enabled
	 * in the "Settings" object with remotes that are actually
	 * defined as mirrors in the Git config file.
	 * @throws IOException 
	 */
	public final static Collection<String> getConfiguredRemotesEnabledInSettings(ApplicationPropertiesService application_properties_service, Repository repository, Settings settings) throws IOException {
		Map<String, String> mirrors = getRepoRemotes(application_properties_service, repository);
		Map<String, Boolean> enabled_mirrors = ApiConfigurationResource.getMirrorsEnabled(settings, mirrors);

		Collection<String> enabled_remotes = new ArrayList<String>();
		for (Entry<String, Boolean> entry : enabled_mirrors.entrySet())
			if (entry.getValue())
				enabled_remotes.add(entry.getKey());
				
		return enabled_remotes;
	}
	
	/**
	 * Simply pushes to the enabled mirror(s).
	 */
	@Override
	public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {

		try {
			Collection<String> enabled_remotes = getConfiguredRemotesEnabledInSettings(this.applicationPropertiesService, context.getRepository(), context.getSettings());
			pushToRemotes(this.gitCommandBuilderFactory, context.getRepository(), enabled_remotes);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static StoredConfig getConfig(ApplicationPropertiesService application_properties_service, Repository repository) throws IOException {
		File repo_dir = application_properties_service.getRepositoryDir(repository);
		org.eclipse.jgit.lib.Repository repo = new FileRepositoryBuilder().setGitDir(repo_dir).build();
		StoredConfig config = repo.getConfig();
		repo.close();
		return config;
	}

	public static Map<String, String> getRepoRemotes(ApplicationPropertiesService application_properties_service, Repository repository) throws IOException {

		StoredConfig git_config = getConfig(application_properties_service, repository);

		Map<String, String> named_remotes_with_mirroring = new HashMap<String, String>();
		Map<String, String> named_remotes_without_mirroring = new HashMap<String, String>();
		for (String subsection_name : git_config.getSubsections(REMOTES_CONFIG_SECTION_NAME)) {
			boolean mirroring = git_config.getBoolean(REMOTES_CONFIG_SECTION_NAME, subsection_name, "mirror", false);
			String url = git_config.getString(REMOTES_CONFIG_SECTION_NAME, subsection_name, KEY_REMOTES_URL);
			if (mirroring)
				named_remotes_with_mirroring.put(subsection_name, url);
			else
				named_remotes_without_mirroring.put(subsection_name, url);
		}
		
		return named_remotes_with_mirroring;
	}

	// FIXME
	public static Map<String, String> testRepoRemote(
			ApplicationPropertiesService application_properties_service,
			Repository stash_repository) {

		Map<String, String> results = new HashMap<String, String>();
		results.put("foo", "bar");
		return results;
	}

	public static void addRepoRemote(ApplicationPropertiesService application_properties_service, Repository repository, String remote_name, String remote_url) throws IOException {
		StoredConfig git_config = getConfig(application_properties_service, repository);
		git_config.setString(REMOTES_CONFIG_SECTION_NAME, remote_name, KEY_REMOTES_URL, remote_url);
		git_config.setBoolean(REMOTES_CONFIG_SECTION_NAME, remote_name, KEY_REMOTES_MIRROR, true);
		git_config.save();
	}

	public static void removeRepoRemote(ApplicationPropertiesService application_properties_service, Repository repository, String remote_name) throws IOException {
		StoredConfig git_config = getConfig(application_properties_service, repository);
		git_config.unsetSection(REMOTES_CONFIG_SECTION_NAME, remote_name);
		git_config.save();
	}
}