Force Push Branch Portector
============================

Prevents force pushes to branches specified by a config file. The hook configuration lets
you choose the branch and path at which the config file resides.

You may want to create an orthogonal branch in your repository with its own branch permissions
(e.g. admins only) to store this config file.

A suggested name for the hook file is `forcepush.config`.

The config file format is a list of regular expressions that will be tested against branch
names for force push rejection.

An example file:

    refs/heads/master
    refs/heads/another-specific-branch
    refs/heads/noforce/.*

Note that the lines represet *regular expressions*, not *glob expressions*. The following line won't
do what you expect:

    refs/heads/noforce/*

You need the period, like this:

    refs/heads/noforce/.*

TODO
============================

* Re-instate unit tests


Acknowledgements
============================

[This project on Bitbucket](https://bitbucket.org/mark0815/stash-reject-force-push-hook) was used as a template to get started.
