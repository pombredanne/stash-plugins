package com.kostmo.stash.hooks.forcepush;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.kostmo.stash.hooks.forcepush.RefValidator.FieldError;

public class StashUtils {
	public static org.eclipse.jgit.lib.Repository getJGitRepoFromStashRepo(ApplicationPropertiesService applicationPropertiesService, Repository stash_repo) throws IOException {
		
		File repo_dir = applicationPropertiesService.getRepositoryDir(stash_repo);
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		org.eclipse.jgit.lib.Repository repository = builder.setGitDir(repo_dir).build();
		return repository;	
	}
	
	public static InputStream getInputStreamFromSettings(org.eclipse.jgit.lib.Repository jgit_repository, Settings settings, String branch_field, String path_field) throws FieldError, MissingObjectException, IOException {

		String config_file_branch = settings.getString(branch_field);
		if (config_file_branch == null)
			throw new FieldError(branch_field, "Field cannot be null.");

		ObjectId lastCommitId = jgit_repository.resolve(config_file_branch);
		if (lastCommitId == null)
			throw new FieldError(branch_field, "Invalid refspec");

		String config_file_path = settings.getString(path_field);
		if (config_file_path == null || config_file_path.length() == 0)
			throw new FieldError(path_field, "Field cannot be null or empty.");

		InputStream is;
		try {
			is = GitUtils.getPathInputStreamAtRevision(jgit_repository, lastCommitId, new File(config_file_path));
			if (is == null)
				throw new FieldError(path_field, String.format("Could not read a file at path \"%s\" at ref \"%s\".", config_file_path, config_file_branch));
		} catch (IllegalArgumentException e) {
			throw new FieldError(path_field, e.getMessage());
		}

		return is;
	}
}
