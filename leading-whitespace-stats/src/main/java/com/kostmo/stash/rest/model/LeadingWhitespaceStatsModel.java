package com.kostmo.stash.rest.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class LeadingWhitespaceStatsModel {

    @JsonProperty
    final private int pure_tabs_count;
    
    @JsonProperty
    final private int pure_spaces_count;

    @JsonProperty
    final private int mixed_indentation_count;
    
    public LeadingWhitespaceStatsModel(int pure_tabs_count, int pure_spaces_count, int mixed_indentation_count) {
        this.pure_tabs_count = pure_tabs_count;
        this.pure_spaces_count = pure_spaces_count;
        this.mixed_indentation_count = mixed_indentation_count;
    }
}